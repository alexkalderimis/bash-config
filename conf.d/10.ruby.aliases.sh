# Ruby development aliases
# alias bundt="bundle _${CURRENT_RB_BUNDLER}_ exec"
alias bundt="gl-bundle-exec"
alias handbook-run="bundle exec middleman"
alias hbook="bundle exec middleman"
alias glspec="be rspec"
alias rspec="be rspec"
alias glcop="be rubocop"
alias rcop="be rubocop"
alias migrate="bundt rails db:migrate RAILS_ENV=test"
alias railsc="bundt rails c"
# alias bundt-deps="bundle _${CURRENT_RB_BUNDLER}_ install"
alias bundt-deps="bundle install"
alias be='gl-bundle-exec'
alias rails='be rails'
alias test-prof="FDOC=1 FPROF=1 EVENT_PROF='sql.active_record' glspec --profile --"

alias rtags="ctags -R --languages=ruby --exclude=.git --exclude=log ."

ff-on() {
  rails runner "Feature.enable(:$1); puts %Q{enabled :$1}"
}

ff-off() {
  rails runner "Feature.disable(:$1); puts %Q{disabled :$1}"
}

ff() {
  rails runner "puts %Q{FF :$1 enabled? #{Feature.enabled?(:$1)}}"
}
