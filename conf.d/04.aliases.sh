# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias ls="ls -G"

# List processes and the ports they are bound to
alias ls-net="netstat -lnp" 

# if hash yarnpkg 2>/dev/null; then
#   alias yarn=yarnpkg
# fi

if has entr; then
   alias watch-ee='find ee -name "*.rb" | entr -n -c glspec'
fi

find_dag() {
  ss -l -p -t | grep "$1" | sed 's/   */;/g' | cut -d ';' -f 6-
}

kill-port-holder() {
  find_dag "$1" | perl -M5.28.0 -ne '/pid=(\d+)/; say $1' | xargs kill -9
}
