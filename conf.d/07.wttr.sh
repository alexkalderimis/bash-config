wttr_places="Cambridge%20UK,Washington%20DC,Wellington%20NZ"

alias wttr="wttr_in"
alias vttr="curl http://v2.wttr.in/"

alias wttrs="curl --compressed -s 'https://wttr.in/{$wttr_places}?m&format=3'"

wttr_in() {
  if [[ "$1" == "--help" || "$1" == "-h" ]]; then
    curl --compressed "http://wttr.in/:help"
  else
    curl --compressed "http://wttr.in/$1?m&format=${2}"
  fi
}
