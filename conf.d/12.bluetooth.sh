HEADPHONES_MAC='4C:87:5D:2B:E4:B5'
alias restart-bluetooth="sudo systemctl restart bluetooth.service"
alias headphones="bluetoothctl connect ${HEADPHONES_MAC}"
alias nophones='bluetoothctl disconnect'
