export GOBIN="$LOCAL_BIN"
export GO111MODULE="on"

alias mkgo="GOBIN=$LOCAL_BIN GO111MODULE=on go install"
