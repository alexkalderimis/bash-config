export PATH=$LOCAL_BIN:./bin:$PATH

if hash asdf 2>/dev/null; then
  export PATH=$PATH:$(asdf where golang)/packages/bin
fi

export PATH=$PATH:$HOME/.config/bash/scripts
