ASDF_SH="$HOME/.asdf/asdf.sh"
[[ -s $ASDF_SH ]] && source $ASDF_SH

ASDF_COMPLETIONS="$HOME/.asdf/completions/asdf.bash"
[[ -s $ASDF_COMPLETIONS ]] && source $ASDF_COMPLETIONS
