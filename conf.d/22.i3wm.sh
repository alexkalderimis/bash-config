# Needed to configure evolution to use gsuite accounts
# See:
#   https://sakhnik.com/2018/08/02/i3-gnome.html
alias control-center="env XDG_CURRENT_DESKTOP=GNOME gnome-control-center"
