#!/usr/bin/env bash

_reassign_completions() {
  key="$(git config --get remote.origin.url)/$(current-branch)"
  file=$( echo "$key" | sha256sum | cut -d ' ' -f 1)
  cache="$HOME/.cache/scripts/reassign.complete"

  mkdir -p "$cache"

  cache_file="$cache/$file"

  if [[ -n $(find $cache/ -name $file -mmin -60) ]] ; then
    candidates=$(cat $cache_file)
  else
    candidates=$(participants | tee $cache_file)
  fi

  COMPREPLY=($(compgen -W "$(echo $candidates | tr " " "\n" | grep -i "${COMP_WORDS[1]}")"))
}

complete -F _reassign_completions reassign
