#!/usr/bin/env bash

_assign_completions() {
  COMPREPLY=($(compgen -W "$(find-user ${COMP_WORDS[1]:0:3} bare | tr " " "\n" | grep -i "${COMP_WORDS[1]}")"))
}

complete -F _assign_completions assign

