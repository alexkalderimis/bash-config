#!/bin/env ruby

require 'yaml'
require 'optparse'
require 'pathname'
require 'pp'
require "active_support"
require "active_support/core_ext"

DATA_DIR = Pathname.new("#{ENV['HOME']}/projects/gitlab/www-gitlab-com/data")

def split_map(map, keys)
  new = map.slice(*keys)
  old = map.reject { |k, v| keys.include?(k) }
  [new, old]
end

persons = DATA_DIR.glob("team_members/person/*/*.yml")
team = persons.map do |path|
  m, data = split_map(YAML.load_file(path), %w[twitter picture country locality])

  data.merge(
    'slug' => path.sub_ext('').basename.to_s,
    'location' => m.values_at('locality', 'country').compact.join(', ')
  )
end

options = {}
fields = []

parser = OptionParser.new do |opts|
  opts.banner = 'Usage: who-the-fox [options]'

  opts.on('-g', '--gitlab username', 'Show a team member by gitlab username') do |username|
    options[:gl_username] = username
  end

  opts.on('-d', '--description DESC', 'Show team members by job-description') do |desc|
    options[:job_description] = desc
  end

  opts.on('-e', '--expertise area', 'Show a team member by expertise') do |area|
    options[:expertise] = area&.downcase
  end

  opts.on('-f', '--field field', 'Show this field') do |field|
    fields << field
  end

  opts.on('-h', '--help', 'Show this message') do
    puts opts
    exit
  end
end

parser.parse!

if options.empty?
  puts parser
  exit 1
end

def unhtml(text)
  p = />(?<role>[^<]+)</
  return text unless p.match?(text)

  text.scan(p).join(' ').gsub(/\s+/, ' ')
end

def justify(text)
  return ['Intl. fox of mystery'] unless text

  words = text.lines.flat_map { |line| line.split(/\s/) }.reverse
  lines = []
  current_line = ''
  while w = words.pop
    if current_line.size > 80
      lines << current_line.chomp
      current_line = ''
    end
    current_line << " #{w}"
  end
  lines << current_line.chomp if current_line != ''

  return lines
end

def show_attribute(team, key, value)
  if value.is_a?(Date)
    value = value.iso8601
  elsif key == 'reports_to'
    key = 'Reports to'
    value = team.find { |p| p[:slug] == value }&.fetch('name') || value
  elsif key == 'role' || key == 'expertise'
    value = unhtml(value)
  elsif value.is_a?(Array)
    puts "#{key}:"
    value.each do |v|
      puts " - #{v}"
    end
    return
  elsif value.is_a?(Hash)
    puts "#{key}:"
    value.each do |k, v|
      puts " #{k}: #{v}"
    end
    return
  elsif key == 'story' || key == 'remote_story'
    puts "#{key}:"
    justify(value).each do |line|
      puts "  #{line}"
    end
    return
  end

  puts "#{key}: #{value.try(:chomp) || value}"
end

def show(team, person)
  return unless person

  basic = person.slice('name', 'role', 'location')
  %w[name role location].each { |k| person.delete(k) }

  basic.each do |k, v|
    show_attribute(team, k, v)
  end
  person.each do |key, value|
    show_attribute(team, key, value)
  end
end

if fields.empty? && !options.key?(:gl_username)
  fields = ['name', 'location', 'role', 'gitlab', 'domain_expertise', 'specialty', 'departments']
end

def selected?(person, gl_username: nil, expertise: nil, job_description: nil)
  return person['gitlab'] == gl_username if gl_username

  if expertise
    person_area = "#{person['specialty']} #{person['domain_expertise']&.join(' ')}".downcase
    return false unless person_area.include?(expertise.downcase)
  end

  if job_description
    return false unless person.fetch('role', '').downcase.include?(job_description.downcase)
  end

  true
end

team.lazy.select { |p| selected?(p, **options) }.each do |person|
  puts("------")
  person = person.slice(*fields) unless fields.empty?
  show(team, person)
end
