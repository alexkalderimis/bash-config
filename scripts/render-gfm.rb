#!/usr/bin/env ruby

require 'net/http'
require 'json'

markdown = STDIN.read

params = {
  text: markdown,
  gfm: true,
  project: ARGV[0]
}

base_url = ARGV[1] || 'http://127.0.0.1:3001'
url = "#{base_url}/api/v4/markdown"
uri = URI(url)

req = Net::HTTP::Post.new(uri)
req['Content-Type'] = 'application/json'
req.body = params.to_json

res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: url.start_with?('https')) do |http|
  http.request(req)
end

case res
when Net::HTTPSuccess, Net::HTTPRedirection
  puts JSON.parse(res.body)['html']
else
  puts res.value
end
