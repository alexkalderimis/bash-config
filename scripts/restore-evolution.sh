#!/bin/bash

tmp_dir="/tmp/evolution-mail-restore/$(date --iso-8601=date)"
rm -rf $tmp_dir
mkdir -p $tmp_dir

cd $tmp_dir
tar xvf ~/backup/evolution-mail-backup-latest.tar.gz

cd $tmp_dir/config
if [[ -d ~/.config/evolution ]] ; then
  mv ~/.config/evolution ~/.config/evolution.old
fi
mv evolution/ ~/.config/

cd $tmp_dir/local
if [[ -d ~/.local/share/evolution ]] ; then
  mv ~/.local/share/evolution ~/.local/share/evolution.old
fi
mv evolution/ ~/.local/share/

cd

rm -rf $tmp_dir/
