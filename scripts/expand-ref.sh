#!/bin/bash

shopt -s extglob
set -e

mr-url() {
  ref="$1"
  iid=$(echo -n $ref | sed 's/!//g')

  lab mr show $iid -j | jq -r  .WebURL
}

issue-url() {
  ref="$1"
  iid=$(echo -n $ref | sed 's/#//g')

  lab issue show $iid | grep WebURL | cut -d ' ' -f2 -
}

while read line
do
  case "$line" in
    \!+([0-9]))
      mr-url $line
      ;;
    \#+([0-9]))
      issue-url $line
      ;;
    *)
      echo $line
      ;;
  esac
done < "${1:-/dev/stdin}"
