#!/usr/bin/env stack
{- stack
  script
  --resolver lts-15.13
  --package unordered-containers
  --package text
  --package containers
  --
  +RTS -s -RTS
-}

{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Traversable
import qualified Data.Graph as G
import qualified Data.Map.Strict as M
import qualified Data.Text as T
import qualified Data.Text.IO as T

type Edge = (Text, Text, [Text])

main :: IO ()
main = do
  input <- T.lines <$> T.getContents
  guard (not $ null input)

  let (g, unvertex, _) = G.graphFromEdges (edges input)

  for_ (zip [1..] (G.components g)) $ \(i, component) -> do
    putStrLn (show i <> ":")
    putStrLn "---------------"
    mapM_ (\v -> let (fn, _, _) = unvertex v in T.putStrLn fn) (toList component)
    putStrLn ""

edges :: [Text] -> [Edge]
edges = fromMap . foldl' combine M.empty . (>>= toEdge)
  where
    combine m (t, mt) = M.insertWith (<>) t (maybeToList mt) m
    fromMap m = [(t, t, ts) | (t, ts) <- M.toList m]

toEdge :: T.Text -> [(Text, Maybe Text)]
toEdge t = let (name, rest) = T.breakOn "->" t
            in case (T.strip name, T.strip (T.drop 2 rest)) of
                 ("", _) -> []
                 (name', "") -> [(name', Nothing)]
                 (name', rest') -> [(name', Just rest'), (rest', Nothing)]

-- vim: set filetype=haskell:
