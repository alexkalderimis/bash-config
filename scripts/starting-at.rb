#!/usr/bin/env ruby

start_at = ARGV.shift

ARGF.each_line
  .map(&:chomp)
  .drop_while { |line| start_at == ':all' ? false : !line.include?(start_at) }
  .each { |line| puts line }
