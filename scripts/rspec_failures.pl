#!/usr/bin/perl -w

use strict;
use v5.10;

my $pattern = qr{^rspec '?(?<file>./(\S+).rb)[:\[]'?.*$};
my %found;

while (<>) {
  next unless $_ =~ $pattern;
  my $file = $+{file};

  next if (!$file || $found{$file});

  say $file;
  $found{$file} = 1;
}
