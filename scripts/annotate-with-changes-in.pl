#!/usr/bin/perl -w

use strict;
use v5.10;

my $pattern = qr{(?<sha>[0-9a-f]{11}).*$};

while (<>) {
  next unless $_ =~ $pattern;
  my $sha = $+{sha};

  print $_;

  open(my $fh, '-|', "git changes-in $sha") or die $!;
  while (my $line = <$fh>) {
    print "\t$line";
  }
}
