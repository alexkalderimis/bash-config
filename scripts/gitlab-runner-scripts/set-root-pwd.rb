puts '=' * 80

pwd = ARGV[0]

raise 'No password. Please supply a password' unless pwd

root = User.find_by(username: 'root')

raise 'No root user! run db:seed_fu' unless root

root.password = pwd
root.save!

puts "Password updated"
