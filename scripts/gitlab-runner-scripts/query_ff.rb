length = ARGV.map(&:length).max

puts '=' * 80
puts "#{'flag'.ljust(length + 1)} | enabled?"
puts(('-' * (length + 2)) +      '|---------')

ARGV.each do |flag|
  puts %Q(:#{flag.ljust(length)} | #{Feature.enabled?(flag.to_sym)})
end
