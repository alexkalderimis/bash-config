require 'json'

resp = IO.popen(['lab', 'graphql', <<~EPIC_QUERY, '-v', "path:gitlab-org", '-v', 'iid:7652'])
query($iid: ID!, $path: ID!) {
  group(fullPath: $path) { epic(iid: $iid) { id } }
}
EPIC_QUERY

epic = JSON.load(resp.gets)['group']['epic']

create_issue_query = <<~GQL
mutation($desc: String!, $title: String!) {
  createIssue(input: {
    description: $desc,
    title: $title,
    projectPath: "gitlab-org/gitlab",
    labels: ["group::integrations", "type::maintenance"],
    epicId: "#{epic['id']}"
    }) {
    issue { id }
    errors
  }
}
GQL


puts epic.inspect

Dir.glob('app/models/integrations/*.rb').each do |filename|
  puts "=> #{filename}"
  print "create issue? [Ny] "

  if gets =~ /y(es)?/i
    title = "Static Integration DSL: #{filename}"
    description = <<~DESC
      Convert #{filename} to use the static field DSL introduced
      in https://gitlab.com/gitlab-org/gitlab/-/issues/353822
    DESC

    io = IO.popen(['lab', 'graphql', create_issue_query, { desc: description, title: title }.to_json])

    r = JSON.load(io.gets)

    if r.dig('createIssue', 'errors')&.any?
      r.dig('createIssue', 'errors').each { $stderr.puts(_1) }
    else
      puts "Created #{r.dig('createIssue', 'issue', 'id')}"
    end
  end
end
