#!/bin/bash

BORG_FILE_LIST=()

# Load keychain variables and check for id_rsa
[ -z "$HOSTNAME" ] && HOSTNAME=`uname -n`
[ -f $HOME/.keychain/$HOSTNAME-sh ] && \
  . $HOME/.keychain/$HOSTNAME-sh 2>/dev/null
ssh-add -l 2>/dev/null | grep -q RSA || exit 1

borg-setup() {
  case "$(uname)" in
    Darwin)
      if [ -z "$BORG_PASSCOMMAND" ] ; then
        security add-generic-password -D secret -U -a $USER -s borg-passphrase -w $(head -c 1024 /dev/urandom | base64)
      fi
      export BORG_PASSCOMMAND="security find-generic-password -a $USER -s borg-passphrase -w"
      ;;
    *)
      if test -e "$HOME/.borg-passphrase" ; then
        : # pass
      else
        head -c 1024 /dev/urandom | base64 > "$HOME/.borg-passphrase"
        chmod 400 "$HOME/.borg-passphrase"
      fi
      export BORG_PASSCOMMAND="cat $HOME/.borg-passphrase"
      ;;
  esac
}

populate_borg_file_list() {
  BORG_FILE_LIST+=( "${BORG_FILE_LIST_LOCAL[@]}" )

  BORG_FILE_LIST+=(
   "$HOME/.*codes" \
   "$HOME/.*token" \
   "$HOME/.*tokens" \
   "$HOME/.*recovery-tokens" \
   "$HOME/.*2fa-codes" \
   "$HOME/.NERDTreeBookmarks" \
   "$HOME/.adventofcode" \
   "$HOME/.asdf" \
   "$HOME/.bash_history" \
   "$HOME/.bash_profile" \
   "$HOME/.bashrc" \
   "$HOME/.borg*" \
   "$HOME/.bundle/config" \
   "$HOME/.byobu" \
   "$HOME/.config" \
   "$HOME/.emulator_console_auth_token" \
   "$HOME/.env-vars" \
   "$HOME/.exercism.json" \
   "$HOME/.face" \
   "$HOME/.face.icon" \
   "$HOME/.fonts.conf" \
   "$HOME/.*{rc,conf,yml,json}" \
   "$HOME/.gdk.yml" \
   "$HOME/.ghc/ghci_history" \
   "$HOME/.ghci" \
   "$HOME/.gnupg" \
   "$HOME/.guard_history" \
   "$HOME/.hgrc" \
   "$HOME/.hledger-web_client_session_key.aes" \
   "$HOME/.hledger.journal" \
   "$HOME/.hledger.journal.2017.04.01.bak" \
   "$HOME/.irb-history" \
   "$HOME/.irssi" \
   "$HOME/.jsonresume.json" \
   "$HOME/.lambdabot" \
   "$HOME/.lein/profiles.clj" \
   "$HOME/.lein/repl-history" \
   "$HOME/.lein_history" \
   "$HOME/.lesshst" \
   "$HOME/.mkshrc" \
   "$HOME/.netrc" \
   "$HOME/.nix-channels" \
   "$HOME/.node_repl_history" \
   "$HOME/.profile" \
   "$HOME/.pry_history" \
   "$HOME/.psql_history" \
   "$HOME/.python_history" \
   "$HOME/.rvmrc" \
   "$HOME/.sh_history" \
   "$HOME/.ssh" \
   "$HOME/.stack" \
   "$HOME/.tmux.conf" \
   "$HOME/.tool-versions" \
   "$HOME/.travis" \
   "$HOME/.viminfo" \
   "$HOME/.yarnrc" \
   "$HOME/.zlogin" \
   "$HOME/.zshrc" \
   "$HOME/.Xmodmap" \
   "$HOME/.local/share/fonts" \
   "$HOME/.local/share/wallpapers" \
   "$HOME/.local/bin" \
   "$HOME/.screenlayout" \
   "$HOME/Desktop" \
   "$HOME/Documents" \
   "$HOME/Downloads" \
   "$HOME/Movies" \
   "$HOME/Music" \
   "$HOME/Pictures" \
   "$HOME/Public" \
   "$HOME/accounts" \
   "$HOME/cc" \
   "$HOME/citeproc" \
   "$HOME/conferences" \
   "$HOME/examples" \
   "$HOME/exercism" \
   "$HOME/from-moa" \
   "$HOME/greek" \
   "$HOME/meetings" \
   "$HOME/notes" \
   "$HOME/projects" \
   "$HOME/rides" \
   "$HOME/scratch" \
   "$HOME/sd-card-old" \
   "$HOME/screensaver-photos" \
   "$HOME/scripts" \
   "$HOME/software" \
   "$HOME/talks" \
   "$HOME/technical-interviews" \
   "$HOME/tools" \
   "$HOME/wikis" \
   )

   if [ "$(uname)" == "Darwin" ]; then
      BORG_FILE_LIST+=( \
         "$HOME/Library/Fonts" \
         "$HOME/Library/KeyBindings" \
         "$HOME/Library/Keyboard Layouts" \
        )
   fi
}

borg-init() {
  if [ -z "$BORG_REPO" ] ; then
    >&2 echo "backup error: No repo defined. Ensure BORG_REPO is in the environment"
    exit 1
  fi
  if [ -z "$BORG_PASSCOMMAND" ] ; then
    borg-setup
  fi

  borg init --encryption=repokey "$BORG_REPO"
}

borg-backup() {
  if [ -z "$BORG_REPO" ] ; then
    >&2 echo "backup error: No repo defined. Ensure BORG_REPO is in the environment"
    exit 1
  fi
  if [ -z "$BORG_PASSCOMMAND" ] ; then
    borg-setup
  fi

  NOW=$(date +"%Y-%m-%d_%H-%M")
  BACKUP="${BORG_REPO}::${NOW}"
  BORG_FILE_LIST=()
  populate_borg_file_list

  borg create -v --stats -C zlib --list \
              --filter=AM \
              --exclude-from ~/.borg-filter \
              "$BACKUP" ${BORG_FILE_LIST[@]}

  borg prune -v --list "$BORG_REPO" --keep-daily=3 \
                                    --keep-weekly=4 \
                                    --keep-monthly=12
}

borg-list() {
  if [ -z "$BORG_PASSCOMMAND" ] ; then
    borg-setup
  fi
  borg list "$BORG_REPO"
}

case "$1" in
  backup)
    borg-backup
    ;;
  init)
    borg-init
    ;;
  setup)
    borg-setup
    ;;
  list)
    borg-list
    ;;
  *)
    >&2 echo "usage: $0 backup|setup"
    exit 2
    ;;
esac
