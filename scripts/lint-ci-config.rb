#!/bin/env ruby

require 'json'
require 'pathname'

QUERY = <<~GQL
query($path: ID!, $content: String!) {
  ciConfig(projectPath: $path, content: $content) {
    status
    errors
  }
}
GQL

def path_from_url(url)
  if m = /git@gitlab.com:(?<path>\S+)\.git/.match(url)
    return m[:path]
  end

  raise "Could not parse #{url}"
end

ci_config = Pathname.new('./.gitlab-ci.yml')

url = IO.popen(['git', 'remote', 'get-url', 'origin']) { |r| r.read.chomp }
content = ci_config.read

data = { path: path_from_url(url), content: content }

resp = IO.popen(['lab', 'gql', QUERY, data.to_json]) { |r| JSON.parse(r.read) }

status = resp.dig('ciConfig', 'status')
errors = resp.dig('ciConfig', 'errors')

case status
when 'VALID'
  puts "CI config is good!"
else
  puts "CI config has errors!"
  errors.each { |err| puts " - #{err}" }
  exit 1
end
