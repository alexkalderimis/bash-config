#!/bin/bash

git changes-in origin/master.. \
    | xargs ls -1 2>/dev/null \
    | grep ".rb$" \
    | grep -v "db/schema.rb" \
    | xargs --no-run-if-empty bundle exec rubocop
