#!/usr/bin/perl -w

use strict;
use v5.10;

my $pattern = qr{^rspec \s+ (?<location>\S+) \s+ \# .*$}x;

while (<>) {
  next unless $_ =~ $pattern;
  my $location = $+{location};

  print $location, "\n";
}
