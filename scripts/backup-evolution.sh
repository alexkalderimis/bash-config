#!/bin/bash

stamp=$(date --iso-8601=date)
dir_name="evolution-mail-backup-$stamp"

mkdir -p ~/backup
mkdir -p /tmp/$dir_name/config
mkdir -p /tmp/$dir_name/local

cp -r ~/.config/evolution/ /tmp/$dir_name/config/
cp -r ~/.local/share/evolution/ /tmp/$dir_name/local/

rm -r ~/backup/$dir_name.tar.gz
tar -czvf ~/backup/$dir_name.tar.gz -C /tmp/$dir_name config local

rm -f ~/backup/evolution-mail-backup-latest.*
ln -s ~/backup/$dir_name.tar.gz \
      ~/backup/evolution-mail-backup-latest.tar.gz
